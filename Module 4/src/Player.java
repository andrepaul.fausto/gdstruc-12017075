import java.util.Objects;

public class Player 
{
    private int playerId;
    private String userName;
    

    public Player() {
    }

    public Player(int playerId, String userName) 
    {
        this.playerId = playerId;
        this.userName = userName;
    }

    public Player(int playerId) 
    {
        this.playerId = playerId;
        this.userName = nameGen();
    }

    public int getPlayerId() 
    {
        return this.playerId;
    }

    public void setPlayerId(int playerId) 
    {
        this.playerId = playerId;
    }

    public String getUserName() 
    {
        return this.userName;
    }

    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public Player playerId(int playerId) 
    {
        setPlayerId(playerId);
        return this;
    }

    public Player userName(String userName) 
    {
        setUserName(userName);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if(o == this)
            return true;
        if(!(o instanceof Player)) {
            return false;
        }
        Player player = (Player) o;
        return playerId == player.playerId && Objects.equals(userName, player.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, userName);
    }

    @Override
    public String toString() {
        return "{" +
            " playerId='" + getPlayerId() + "'" +
            ", userName='" + getUserName() + "'" +
            "}";
    }

    public String nameGen()
    {
        switch(playerId)
        {
            case 0:
                userName = "Kenny Omega, Best Bout Machine";
                break;
            case 1:
                userName = "Mr. Brodie Lee, The Exalted One";
                break;
            case 2:
                userName = "Malakai Black";
                break;
            case 3:
                userName = "Matt Jackson, of the Super Kliq";
                break;
            case 4:
                userName = "Nick Jackson, of the Super Kliq";
                break;
            case 5:
                userName = "Miro, The Redeemer";
                break;
            case 6:
                userName = "Adam Cole, of the Super Kliq";
                break;
            case 7:
                userName = "Trent?, of the Best Friends";
                break;
            case 8:
                userName = "Chuck Taylor, of the Best Friends";
                break;
            case 9:
                userName = "CM Punk, Best in the World";
                break;
            case 10:
                userName = "Dax Hardwood, of the FTR";
                break;
            case 11:
                userName = "Orange Cassidy, of the Best Friends";
                break;
            case 12:
                userName = "Fuego Del Sol";
                break;
            case 13:
                userName = "Penta El Zero Miedo, of the Lucha Brothers";
                break;
            case 14:
                userName = "Rey Fenix, of the Lucha Brothers";
                break;
            case 15:
                userName = "John Silver, of the Dark Order";
                break;
        }
        return userName;
    }

}
