public class App 
{
    public static void main(String[] args) throws Exception 
    {
        //instantiating the Array queue with the maximum of 10
        ArrayQueue queue = new ArrayQueue(10);
        //maximum game rounds for the program
        int maximumGameRounds = 10;
        //rounds for the matchmaking
        for(int i = 1; i <= maximumGameRounds; i++)
        {
            //starting players for the matchmaking
            for(int k = 0; k < 5; k++)
            {
                queue.add(new Player(i));
            }
            //to display the current rounds
            System.out.println("Round: " + i);

            playRound(queue);

            //pause the program each round
            System.out.println("Press Any Key To Continue...");
            new java.util.Scanner(System.in).nextLine();
        }
    }
    public static void playRound(ArrayQueue queue)
    {
        System.out.println("The players that joined are: ");
        //queue the players
        for (int k = 0; k < Math.random() * (7 - 1+ 1)+ 1; k++)
        {
            queue.add(new Player(k));
        }
        //print the queued players
        queue.printQueue();
        //pop or remove the first 5 players from the queue
        for(int k = 0; k < 5; k++)
        {
            queue.remove();
        }
        //print the remaining players
        System.out.println("The players that remain are: ");
        queue.printQueue();
    }
}
