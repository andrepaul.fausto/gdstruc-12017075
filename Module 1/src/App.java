import java.util.Random;

public class App 
{  
    //enum in order to determine whether the order of the selection sort would be ascending or descending  
    public enum order
    {
        ASCENDING, 
        DESCENDING
    }
    //to print the array using for loop
    public static void printArray(int[] array)
    {
        for (int i = 0; i < array.length; i++)
            System.out.print (array[i] + " ");
        System.out.println();
    }
    //to generate random numbers for the array
    public static int randomElements()
    {
        Random number = new Random();
        int min = -100;
        int max = 200;
        int rng = min + number.nextInt(max);
        return rng;
    }
    //to generate array in tandem with the randomElements function
    public static int[] arrayGenerator(int indeces)
    {
        int[] array = new int[indeces];
        for (int i = 0; i < array.length; i++)
        {
            array[i] = randomElements();
        }
        return array;
    }
    //Item 1
    public static void bubbleSort(int[] array)
    {
        //To track the array
        for (int lastSortedIndex = 0; lastSortedIndex < array.length; lastSortedIndex++)
        {
            //to iterate over the array
            for(int i = 1; i < (array.length - lastSortedIndex); i++)
            {
                //to arrange the array > makes the function descending order
                if (array[i] > array[i-1])
                {
                    int temp = array[i-1];
                    array[i-1] = array[i];
                    array[i] = temp;
                }
            }
        }
    }
    //to arrange the array on the selection sort
    public static void selectionSortArranger(int[] array, int indexArranger, int lastSortedIndex, order sort)
    {
        //a is for ascending
        if (sort == order.ASCENDING)
        {
            for (int i = lastSortedIndex + 1; i < array.length; i++)
            {
                //determinant whether the array is in descending or ascending order
               if (array[i] < array[indexArranger])
               indexArranger = i;
            } 
        }
        //d is for descending
        if (sort == order.DESCENDING)
        {
            for (int i = lastSortedIndex + 1; i < array.length; i++)
            {
                //determinant whether the array is in descending or ascending order
               if (array[i] > array[indexArranger])
               indexArranger = i;
            } 
        }
        int temp = array[indexArranger];
        array[indexArranger] = array[lastSortedIndex];
        array[lastSortedIndex] = temp;
    }
    public static void selectionSort(int[] array)
    {
        //to scan the whole array
        for (int lastSortedIndex = 0; lastSortedIndex < array.length; lastSortedIndex++)
        {
            //smallestIndex as the tracker 
            int smallestIndex = lastSortedIndex;
            selectionSortArranger(array, smallestIndex, lastSortedIndex, order.DESCENDING);
        }
    }
    //Item 2
    public static void modifiedSelectionSort(int[] array)
    {
        for (int lastSortedIndex = 0; lastSortedIndex < array.length; lastSortedIndex++)
        {
            int smallestIndex = lastSortedIndex;
            //Only difference is the if else for the lastSortedIndex
            //lastSortedIndex should be equals 0 in order to isolate the smallest elemnt
            if (lastSortedIndex == 0)
                selectionSortArranger(array, smallestIndex, lastSortedIndex, order.ASCENDING);
            //the regular selection sort
            else 
                selectionSortArranger(array, smallestIndex, lastSortedIndex, order.DESCENDING);
        }
    }
    public static void main(String[] args) throws Exception 
    {
        //item 1: completed
        int[] numbers = arrayGenerator(15);
        
        System.out.println("Before the Bubble Sort:");
        printArray(numbers);
        System.out.println();

        bubbleSort(numbers);

        System.out.println("After the Bubble Sort:");
        printArray(numbers);
        System.out.println();
        
        int[] numbers2 = arrayGenerator(15);

        System.out.println("Before the Selection Sort:");
        printArray(numbers2);
        System.out.println();

        selectionSort(numbers2);

        System.out.println("After the Selection Sort:");
        printArray(numbers2);
        System.out.println();
        //item 2: completed
        int[] numbers3 = arrayGenerator(15);

        System.out.println("Before the Modified Selection Sort:");
        printArray(numbers3);
        System.out.println();

        modifiedSelectionSort(numbers3);

        System.out.println("After the Modified Selection Sort:");
        printArray(numbers3);
    }
}
