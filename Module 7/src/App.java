public class App {
    public static void main(String[] args) throws Exception 
    {
        Tree tree = new Tree();
        //insert values in the tree
        tree.insert(20);
        tree.insert(-10);
        tree.insert(-7);
        tree.insert(32);
        tree.insert(-37);
        tree.insert(64);
        tree.insert(40);
        tree.insert(11);
        tree.insert(19);
        tree.insert(4);

        //tree.traversalInOrder();
        //print the default state of the tree
        System.out.println("Default state of the tree");
        System.out.println();
        tree.traverseInOrderAscending();
        System.out.println();
        System.out.println("Smallest value in the tree: " + tree.getMin());
        System.out.println();
        System.out.println("Largest value in the tree: " + tree.getMax());
        System.out.println();
        System.out.println("Descending state of the tree");
        System.out.println();
        tree.traverseInOrderDescending();
    }
}
