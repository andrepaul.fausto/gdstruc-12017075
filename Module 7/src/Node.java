import java.util.Objects;

public class Node 
{
    private int data;
    private Node rightChild;
    private Node leftChild;

    public Node(int _data) {
        this.data = _data;
    }

    public void traverseInOrderAscending()
    {
        //tree travels to the left to get all of the small nodes
        if (leftChild != null) 
        {
            leftChild.traverseInOrderAscending();
        }
        //prints the smallest nodes first
        System.out.println("Data: " + data);
        //prints the larger nodes after the small nodes
        if (rightChild != null) 
        {
            rightChild.traverseInOrderAscending();
        }
    }

    public void traverseInOrderDescending()
    {
        //tree travels to the right to get all of the big nodes
        if (rightChild != null) 
        {
            rightChild.traverseInOrderDescending();
        }
        //prints the biggest nodes first
        System.out.println("Data: " + data);
        //prints the smaller nodes after the big nodes
        if (leftChild != null) 
        {
            leftChild.traverseInOrderDescending();
        }
    }

    public void insert(int value) 
    {
        //the first node of the tree or the root of the tree
        if(value == data) {   return;   }
        //smaller values goes to the left of the tree or the left child of the tree
        if (value < data) 
        {
            // if the spot is empty a new node would be instantiated
            if (leftChild == null)
            {
                leftChild = new Node(value);
            }
            //to traverse the tree
            else 
            {
                leftChild.insert(value);
            }
        }
        //bigger value goes to the rightChild of the tree
        else
        {
            // if the spot is empty a new node would be instantiated
            if (rightChild == null)
            {
                rightChild = new Node(value);
            }
            //to traverse the tree
            else 
            {
                rightChild.insert(value);
            }            
        }
    }
    
    public Node get(int value)
    {
        //to get the specific value
        if (value == data) {   return this;   }
        //to get the value from the left child
        if (value < data)
        {
            if (leftChild != null) {   return leftChild.get(value);   }
        }
        //to get the value from the right child
        else
        {
            if(rightChild != null) {   return rightChild.get(value);   }
        }
        //return null to traverse the tree
        return null;
    }

    public int getData() {
        return this.data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getRightChild() {
        return this.rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public Node getLeftChild() {
        return this.leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    public Node data(int data) {
        setData(data);
        return this;
    }

    public Node rightChild(Node rightChild) {
        setRightChild(rightChild);
        return this;
    }

    public Node leftChild(Node leftChild) {
        setLeftChild(leftChild);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Node)) {
            return false;
        }
        Node node = (Node) o;
        return data == node.data && Objects.equals(rightChild, node.rightChild) && Objects.equals(leftChild, node.leftChild);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, rightChild, leftChild);
    }

    @Override
    public String toString() {
        return "{" +
            " data='" + getData() + "'" +
            ", rightChild='" + getRightChild() + "'" +
            ", leftChild='" + getLeftChild() + "'" +
            "}";
    }

}
