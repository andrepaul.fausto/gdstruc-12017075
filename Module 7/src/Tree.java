public class Tree {
    
    private Node root;

    public void insert(int value)
    {
        if (root == null) {   root = new Node(value);   }
        else {   root.insert(value);   }
    }
    //for the tree to access the nodes
    public void traverseInOrderAscending()
    {
        if (root != null)
        {
            root.traverseInOrderAscending();
        }
    }
    //for the tree to access the nodes
    public void traverseInOrderDescending()
    {
        if (root != null)
        {
            root.traverseInOrderDescending();
        }
    }
    //to find the value in the tree
    public Node get(int value)
    {
        //to get the input of the value
        if (root != null)
        {
            return root.get(value);
        }
        //to return if null if the value does not exist
        return null;
    }
    //to gget the smallest value in the tree
    public int getMin()
    {
        //made dummyRoot so that the tree would no be rearranged
        Node dummyRoot = root;
        //to check the tree if it contains a value
        if(dummyRoot == null)
        {
            System.out.println("Tree is empty");
            return -1;
        }
        //traverse the tree and find the minimum value
        while (dummyRoot.getLeftChild() != null)
        {
            //to assign the root the minimum value
            dummyRoot = dummyRoot.getLeftChild();
        }
        return dummyRoot.getData();
    }
    //to get the biggest value in the tree
    public int getMax()
    {
        //made dummyRoot so that the tree would no be rearranged
        Node dummyRoot = root;
        //to check the tree if it contains a value
        if(dummyRoot == null)
        {
            System.out.println("Tree is empty");
            return -1;
        }
        //traverse the tree and find the maximum value
        while (dummyRoot.getRightChild() != null)
        {
            //to assign the root the maximum value
            dummyRoot = dummyRoot.getRightChild();
        }
        return dummyRoot.getData();
    }
}
