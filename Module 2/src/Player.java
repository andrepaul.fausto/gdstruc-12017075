import java.util.Objects;

public class Player 
{
    private int id;
    private String name;
    private int level;

    public Player(int id, String name, int level) 
    {
        this.id = id;
        this.name = name;
        this.level = level;
    }    
    
    public Player(int id) 
    {
        this.id = id;
        this.name = nameGen();
        this.level = levelGen();
    }

    public int getId() {    return this.id;     }

    public String getName() {   return this.name;   }

    public int getLevel() {    return this.level;   }

    public void setId(int id) {     this.id = id;   }

    public void setName(String name) {   this.name = name;   }

    public void setLevel(int level) {   this.level = level;    }
    
    @Override
    public String toString() 
    {
        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            ", level='" + getLevel() + "'" +
            "}";
    }   

    @Override
    public boolean equals(Object o) 
    {
        if (o == this)
            return true;
        if (!(o instanceof Player)) {
            return false;
        }
        Player player = (Player) o;
        return id == player.id && Objects.equals(name, player.name) && level == player.level;
    }

    @Override
    public int hashCode() {     return Objects.hash(id, name, level);   }

    public String nameGen()
    {
        switch(id)
        {
            case 0:
                name = "Kevin";
                break;
            case 1:
                name = "Mark";
                break;
            case 2:
                name = "Jim";
                break;
            case 3:
                name = "David";
                break;
            case 4:
                name = "George";
                break;
            case 5:
                name = "Yoko";
                break;
            case 6:
                name = "Alyssa";
                break;
            case 7:
                name = "Cindy";
                break;
            case 8:
                name = "Miguel";
                break;
            case 9:
                name = "Mary";
                break;
            case 10:
                name = "Maria";
                break;            
        }
        return name;
    }
    
    public int levelGen()
    {
        switch(id)
        {
            case 0:
                level = 52;
                break;
            case 1:
                level = 41;
                break;
            case 2:
                level = 30;
                break;
            case 3:
                level = 61;
                break;
            case 4:
                level = 67;
                break;
            case 5:
                level = 70;
                break;
            case 6:
                level = 39;
                break;
            case 7:
                level = 47;
                break;
            case 8:
                level = 54;
                break;
            case 9:
                level = 64;
                break;
            case 10:
                level = 59;
                break;            
        }
        return level;
    }
}

