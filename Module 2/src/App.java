public class App 
{
    public static void main(String[] args) throws Exception 
    {
        PlayerLinkedList players = new PlayerLinkedList(); 
        //created nodes to the linked list and adding the node to the linked list
        for (int i = 0; i < 10; i++)
        {
            players.addTofront(new Player(i));
        }

        //print the whole list in its default state
        System.out.println("Default Linked List State");
        System.out.println();
        players.printList();
        System.out.println();
        //remove the head 
        players.removeHead();
        //print the list with the head removed
        System.out.println("Linked List with the first Head removed");
        System.out.println();
        players.printList();
        //check if the instance is there 
        System.out.println("Does the element of David exist?");
        boolean contains = players.contains(3, "David", 61);
        if (contains == true)
        {
            System.out.println("true");
        }
        else
        {
            System.out.println("false");
        }
        System.out.println();
        //check what the index of the instance
        System.out.println("Index of Kevin is: " + players.indexOf(0, "Kevin", 52));
    }
}
