public class PlayerLinkedList 
{
    private PlayerNode head;
    
    public void addTofront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }
    public void printList()
    {
        //count for the counter whenever the while loop loops
        int count = 0;
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null)
        {
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
            count++;
        }
        System.out.println("null");
        System.out.println();
        System.out.println("Current amount of nodes present: " + count);
    }
    public void removeHead()
    {
        //created a temporary node to track the head
        PlayerNode playerNode = head;
        //to assign the next player as the new head
        head = head.getNextPlayer();
        playerNode.setNextPlayer(null);
    }
    public boolean contains(int id, String name, int level)
    {
        PlayerNode current = head;
        
        while(current != null)
        {
            //conditional to verify the function parameters
            if ((id == current.getPlayer().getId()) && (name == current.getPlayer().getName()) && (level == current.getPlayer().getLevel()))
            {
                return true;
            }
            //To track the whole list
            current = current.getNextPlayer(); 
        }
        return false;
    }
    public int indexOf(int id, String name, int level)
    {
        int index = 0;
        PlayerNode current = head;
        while(current != null)
        {
            index++;
            //conditional to verify the function parameters
            if ((id == current.getPlayer().getId()) && (name == current.getPlayer().getName()) && (level == current.getPlayer().getLevel()))
            {
                return index;
            }
            //To track the whole list
            current = current.getNextPlayer(); 
        }
        return index;
    }
}
