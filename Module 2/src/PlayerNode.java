public class PlayerNode 
{
    private Player player;
    private PlayerNode nextPlayer;

    public PlayerNode(Player player) {  this.player = player;   }

    public Player getPlayer() { return this.player; }

    public void setPlayer(Player player) {  this.player = player;   }

    public PlayerNode getNextPlayer() { return this.nextPlayer; }

    public void setNextPlayer(PlayerNode nextPlayer) {  this.nextPlayer = nextPlayer;   }
}