public class App 
{
    public static void main(String[] args) throws Exception 
    {
        //Initializing the hash table
        SimpleHashTable hashTable = new SimpleHashTable();
        //Initializing the Players
        Player kenny = new Player(0);
        Player brodie = new Player(1);
        Player malakai = new Player(2);
        Player matt = new Player(3);
        Player nick = new Player(4);
        Player miro = new Player(5);
        Player adam = new Player(6);
        Player trent = new Player(7);
        Player chuck = new Player(8);
        Player punk = new Player(9);
        //Putting the players in the hash table
        hashTable.put(kenny.getUserName(), kenny);
        hashTable.put(brodie.getUserName(), brodie);
        hashTable.put(malakai.getUserName(), malakai);
        hashTable.put(matt.getUserName(), matt);
        hashTable.put(nick.getUserName(), nick);
        hashTable.put(miro.getUserName(), miro);
        hashTable.put(adam.getUserName(), adam);
        hashTable.put(trent.getUserName(), trent);
        hashTable.put(chuck.getUserName(), chuck);
        hashTable.put(punk.getUserName(), punk);

        hashTable.printHashTable();
        System.out.println(hashTable.get("Nick Jackson, of the Super Kliq"));
        System.out.println(hashTable.remove(malakai.getUserName()));
        System.out.println(hashTable.remove(trent.getUserName()));
        hashTable.printHashTable();
    }
}
