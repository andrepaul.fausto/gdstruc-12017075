import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class App 
{
    //to convert a string into an array of characters
    public static char[] stringToCharArray(String words)
    {
        char[] characters = words.toCharArray();
        return characters;
    }
    //To convert a file into a string data type 
    public static String fileToString(String txtFile) throws IOException
    {
        //Path variable to get the .txt file's location or name of the txt file
        Path path = Paths.get(txtFile);
        //Path is now instantiated
        //Using File variable for it to convert the txt into a String
        String contents = Files.readString(path);
        return contents;
    }
    
    public static void main(String[] args) throws Exception 
    {
        //created a new instance of File to pass the file of the .txt
        File txtFile = new File("Steps for the Search Algorithm.txt");
        //created a new instance of Scannerto scan the .txt file
        Scanner scan = new Scanner(txtFile);
        //To read the whole text and display it
        while(scan.hasNextLine())
        {
            System.out.println(scan.nextLine());
        }
        System.out.println();
        //fileContents initializes the txt file as a String
        String fileContents = fileToString("Steps for the Search Algorithm.txt");
        char charArray[] = stringToCharArray(fileContents);
        E eAlgorithm = new E();
        eAlgorithm.E_Method(charArray);
        scan.close();
    }
    
}
