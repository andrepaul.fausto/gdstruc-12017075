public class E
{
    private int eCounter(char[] array)
    {
        int eCounter = 0;
        for (int i = 0; i < array.length; i++)
        {
            if (array[i] == 'e')
            {
                eCounter++;
            }
        }
        return eCounter;
    }

    private int BigECounter(char[] array)
    {
        int eCounter = 0;
        for (int i = 0; i < array.length; i++)
        {
            if (array[i] == 'E')
            {
                eCounter++;
            }
        }
        return eCounter;
    }

    public void E_Method(char[] charArray)
    {
        //Assign each function to a variable to keep track
        int eCounter = eCounter(charArray);
        int capECounter = BigECounter(charArray);
        int totalE = eCounter + capECounter;
        //To display all of the stats that is needed in the E class
        System.out.println();
        System.out.println("The small e's in the text file ammount to: " + eCounter);
        System.out.println("The big E's in the text file ammount to: " + capECounter);
        System.out.println("The total E's in the text file is: " + totalE);
    }
}
