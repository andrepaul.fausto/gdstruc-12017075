import java.util.Objects;

public class Card
{
    private int id;
    private String name;

    public Card(int id, String name) 
    {
        this.id = id;
        this.name = name;
    }

    public Card(int id) 
    {
        this.id = id;
        this.name = nameGen();
    }

    public int getId() {   return this.id;   }

    public void setId(int id) {   this.id = id;   }

    public String getName() {   return this.name;   }

    public void setName(String name) {   this.name = name;   }

    public Card id(int id) 
    {
        setId(id);
        return this;
    }

    public Card name(String name) 
    {
        setName(name);
        return this;
    }

    @Override
    public boolean equals(Object o) 
    {
        if (o == this)
            return true;
        if (!(o instanceof Card)) {
            return false;
        }
        Card card = (Card) o;
        return id == card.id && Objects.equals(name, card.name);
    }

    @Override
    public int hashCode() {   return Objects.hash(id, name);   }

    @Override
    public String toString() 
    {
        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }

    public String nameGen()
    {
        switch(id)
        {
            case 0:
                name = "Kenny Omega, Best Bout Machine";
                break;
            case 1:
                name = "Mr. Brodie Lee, The Exalted One";
                break;
            case 2:
                name = "Malakai Black";
                break;
            case 3:
                name = "Matt Jackson, of the Super Kliq";
                break;
            case 4:
                name = "Nick Jackson, of the Super Kliq";
                break;
            case 5:
                name = "Miro, The Redeemer";
                break;
            case 6:
                name = "Adam Cole, of the Super Kliq";
                break;
            case 7:
                name = "Trent?, of the Best Friends";
                break;
            case 8:
                name = "Chuck Taylor, of the Best Friends";
                break;
            case 9:
                name = "CM Punk, Best in the World";
                break;
            case 10:
                name = "Dax Hardwood, of the FTR";
                break;
            case 11:
                name = "Orange Cassidy, of the Best Friends";
                break;
            case 12:
                name = "Fuego Del Sol";
                break;
            case 13:
                name = "Penta El Zero Miedo, of the Lucha Brothers";
                break;
            case 14:
                name = "Rey Fenix, of the Lucha Brothers";
                break;
            case 15:
                name = "John Silver, of the Dark Order";
                break;
            case 16:
                name = "Alex Reynolds, of the Dark Order";
                break;
            case 17:
                name = "Preston Vance, of the Dark Order";
                break;
            case 18:
                name = "Alan Angels, of the Dark Order";
                break;
            case 19:
                name = "Evil Uno, of the Dark Order";
                break;
            case 20:
                name = "Stu Grayson, of the Dark Order";
                break;
            case 21:
                name = "Colt Cabana, of the Dark Order";
                break;
            case 22:
                name = "Bryan Danielson, The American Dragon";
                break;
            case 23:
                name = "Adam Page, Tha Hangman";
                break;
            case 24:
                name = "Darby Allin";
                break;
            case 25:
                name = "Sting, The Icon";
                break;
            case 26:
                name = "Jon Moxley";
                break;
            case 27:
                name = "Eddie Kingston";
                break;
            case 28:
                name = "Ricky Starks, The Absolute";
                break;
            case 29:
                name = "Maxwell Jacob Friedman, Better than you";
                break;
        }
        return name;
    }
}