import java.util.Random;

public class Main 
{
    public static void main(String[] args) throws Exception 
    {
        LinkedStack deck = new LinkedStack();

        for (int i = 0; i < 30; i++)
        {
            deck.push(new Card(i));
        }
        LinkedStack hand = new LinkedStack();
        LinkedStack discardedPile = new LinkedStack();
        
        System.out.println("Printing the Complete Deck...");       
        deck.printStack();

        while(deck.isEmpty() != true)
        {
            //to pause each loop
            System.out.println("Press Any Key To Continue...");
            new java.util.Scanner(System.in).nextLine();
            stackCommands(deck, hand, discardedPile);
            //printing the decks
            System.out.println("Printing the Deck...");
            deck.printStack();
            System.out.println("Printing the Player's Hand...");
            hand.printStack();
            System.out.println("Printing the Discarded Pile...");
            discardedPile.printStack();
        }
    }
    //discard cards from the deck
    public static void discard(LinkedStack deck, LinkedStack discardPile)
    {
        if(deck.isEmpty() == true)   return;

        discardPile.push(deck.pop());
    }
    //getting cards from the discard pile
    public static void discardCardsToHand(LinkedStack hand, LinkedStack discardPile)
    {
        if(discardPile.isEmpty() == true)   return;
        
        hand.push(discardPile.pop());
    }
    //getting cards from the deck
    public static void drawCards(LinkedStack deck, LinkedStack hand)
    {
        if(deck.isEmpty() == true)   return;
        
        hand.push(deck.pop());
    }
    //to compile the other functions and make the commands only one line of code on the main
    public static void stackCommands(LinkedStack deck, LinkedStack hand, LinkedStack discardPile)
    {        
        Random number = new Random();
        int rng = 1 + number.nextInt(3);
        int rng2 = 1 + number.nextInt(5);
        //to randomize the commands
        switch(rng)
        {
            case 1:
            System.out.println("Discarding Cards..."); 
            for(int i = 1; i < rng2; i++)
            {
                discard(deck, discardPile);
            }
                break;
            case 2:
            System.out.println("Getting Cards from the Deck..."); 
            for(int i = 1; i < rng2; i++)
            {
                drawCards(deck, hand);
            }
                break;
            case 3:
            System.out.println("Getting Cards from the Discarded Pile..."); 
            for(int i = 1; i < rng; i++)
            {
                discardCardsToHand(hand, discardPile);
            }
                break;
        }
    }
}
